package com.team8;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

import com.team8.Product.Parts;
import com.team8.Product.Peripherals;

public class PartsTest {
    @Test
    public void getManufacturerTest() {
        Parts part = new Peripherals("aaa", "eee", 20.1, 60.41, "aea", "Y", "big");
        assertTrue(part.getManufacturer().equals("eee"));
    }

    @Test
    public void getNameTest() {
        Parts part = new Peripherals("aaa", "eee", 20.1, 60.41, "aea", "Y", "big");
        assertTrue(part.getName().equals("aaa"));
    }

    @Test
    public void getPrice() {
        Parts part = new Peripherals("aaa", "eee", 20.1, 60.41, "aea", "Y", "big");
        assertTrue(part.getPrice() == 60.41);
    }
}
