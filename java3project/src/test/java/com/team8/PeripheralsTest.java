package com.team8;

import org.junit.Test;

import com.team8.Product.Peripherals;

import static org.junit.Assert.assertEquals;

public class PeripheralsTest {
    @Test
    public void getTypeTest() {
        Peripherals p = new Peripherals("null", "aaa", 0, 0, "eee", "502", "ytr");
        assertEquals(p.getType(), "502");
    }

    @Test
    public void getConnectionTypeTest() {
        Peripherals p = new Peripherals("null", "aaa", 0, 0, "eee", "502", "ytr");
        assertEquals(p.getConnectionType(), "eee");
    }

    @Test
    public void getWattageTest() {
        Peripherals p = new Peripherals("null", "aaa", 0, 0, "eee", "502", "ytr");
        assertEquals(p.getWattage(), 0, 0.1);
    }

    @Test
    public void getSizeTest() {
        Peripherals p = new Peripherals("null", "aaa", 0, 0, "eee", "502", "ytr");
        assertEquals(p.getFormFactor(), 0, 0);
    }

    @Test
    public void toStringTest() {
        Peripherals p = new Peripherals("null", "aaa", 0, 0, "eee", "502", "ytr");
        assertEquals(p.toString(), "Name: null" + ", Manufacturer: aaa" + ", Price: " + 0.0 + "$, Wattage: " + 0.0
                + ", Connection Type: eee" + ", Type: 502" + ", Size: ytr");
    }

    @Test
    public void toStringDetailsTest() {
        Peripherals p = new Peripherals("null", "aaa", 0, 0, "eee", "502", "ytr");
        assertEquals(p.toStringDetails(),
                "This is a null" + " made by aaa" + ". It uses about " + 0.0 + " watts per hour and costs around " + 0.0
                        + " dollars" + ". You use this product by connecting it to your computer through the eee"
                        + " port. It is a 502" + " device of ytr" + " size format.");
    }
}
