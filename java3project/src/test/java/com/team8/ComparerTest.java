package com.team8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.team8.Comparer.*;
import com.team8.DataManager.FileReader;
import com.team8.Inventory.Inventory;

import Exception.CustomLoaderException;

public class ComparerTest {

    @Test
    public void testCompareType() {
        FileReader file = new FileReader("src\\csv\\Products\\Hardware.csv");
        Inventory inven = new Inventory();

        try {
            inven.addParts(file.loadFileData());
        } catch (CustomLoaderException e) {
        }
        ICompare sortType = new compareType();

        // Hard Drive,Manufacturer2,3.5-inch,10,50.0,(Storage),SATA
        // RAM,Manufacturer1,DDR4,5,80.0,(Memory),DIMM
        int intResultComparaison = sortType.compareParts(inven.getStoragePart(0), inven.getStoragePart(1));
        assertEquals("Comparator should return positive number", true, intResultComparaison > 0);
    }

    @Test
    // help
    public void testCompareManufacturer() {
        FileReader file = new FileReader("src\\csv\\Products\\Hardware.csv");
        Inventory inven = new Inventory();

        try {
            inven.addParts(file.loadFileData());
        } catch (CustomLoaderException e) {
        }
        ICompare SortManu = new compareManufacturer();

        // Hard Drive,(Nvidia),3.5-inch,10,50.0,Storage,SATA
        // RAM,(Manufacturer1),DDR4,5,80.0,Memory,DIMM
        int intResultComparaison = SortManu.compareParts(inven.getStoragePart(0), inven.getStoragePart(1));
        assertEquals("Comparator should return negative number", false, intResultComparaison < 0);
    }

    @Test
    public void testComparePrice() {
        FileReader file = new FileReader("src\\csv\\Products\\Hardware.csv");
        Inventory inven = new Inventory();

        try {
            inven.addParts(file.loadFileData());
        } catch (CustomLoaderException e) {
        }
        ICompare sortPrice = new comparePrice();

        // Hard Drive,Nvidia,3.5-inch,10,(50.0),Storage,SATA
        // RAM,Manufacturer1,DDR4,5,(80.0),Memory,DIMM
        int intResultComparaison = sortPrice.compareParts(inven.getStoragePart(0), inven.getStoragePart(1));
        assertEquals("Comparator should return negative number", true, intResultComparaison < 0);
    }
}
