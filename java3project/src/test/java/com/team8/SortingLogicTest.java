package com.team8;


import static org.junit.Assert.assertEquals;
import org.junit.Test;

import com.team8.Comparer.*;
import com.team8.DataManager.*;
import com.team8.Inventory.Inventory;
import com.team8.Sorting.*;

import Exception.CustomLoaderException;

public class SortingLogicTest {
    
    /**
     * 
     */
    @Test
    public void checkLogicSortType(){
        SortingLogic logic = new SortingLogic();
        FileReader file = new FileReader("src\\csv\\Products\\Hardware.csv");
        Inventory inven= new Inventory();

        try{
        inven.addParts(file.loadFileData());
        }catch(CustomLoaderException e){}
        ICompare sortType = new compareType();
        logic.sortParts(inven.getStorage(), sortType);
        
        file.setFilePath("src\\csv\\Products\\SortedHardwareByType.csv");

        Inventory sortedInven = new Inventory();

        try{
            sortedInven.addParts(file.loadFileData());
        }catch(CustomLoaderException e){}

        assertEquals("Inventory sorted",inven.toString(),sortedInven.toString());
    }
}
