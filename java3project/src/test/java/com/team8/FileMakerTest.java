package com.team8;

import org.junit.Test;

import com.team8.DataManager.FileMaker;
import com.team8.Product.InternalHardware;

public class FileMakerTest {
    @Test
    public void makeFileTest() {
        FileMaker make = new FileMaker();

        InternalHardware ih = new InternalHardware("null", "aaa", "eee", 0, 0.0, "502", "ytr");

        make.makeFile(ih, "src\\csv\\Products\\Test\\Test.csv", "Internal Hardware");

    }
}
