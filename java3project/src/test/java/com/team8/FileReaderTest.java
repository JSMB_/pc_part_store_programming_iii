package com.team8;

import org.junit.Test;

import com.team8.DataManager.FileReader;

import Exception.CustomLoaderException;

public class FileReaderTest {
    @Test
    public void readFile() {
        FileReader reader = new FileReader(
                "java3project\\src\\test\\java\\com\\team8\\file_reader_test_files\\CPU.txt");
        try {
            reader.loadFileData();
        } catch (CustomLoaderException e) {
            e.printStackTrace();
        }
    }
}
