package com.team8;

import org.junit.Test;

import com.team8.Product.InternalHardware;

import static org.junit.Assert.assertEquals;

public class InternalHardwareTest {
    @Test
    public void getTypeTest() {
        InternalHardware ih = new InternalHardware("null", "aaa", "eee", 0, 0.0, "502", "ytr");
        assertEquals(ih.getType(), "502");
    }

    @Test
    public void getConnectionTypeTest() {
        InternalHardware ih = new InternalHardware("null", "aaa", "eee", 0, 0, "502", "ytr");
        assertEquals(ih.getConnectionType(), "ytr");
    }

    @Test
    public void getWattageTest() {
        InternalHardware ih = new InternalHardware("null", "aaa", "eee", 0, 0, "502", "ytr");
        assertEquals(ih.getWattage(), 0, 0.1);
    }

    @Test
    public void getFormFactorTest() {
        InternalHardware ih = new InternalHardware("null", "aaa", "eee", 0, 0, "502", "ytr");
        assertEquals(ih.getFormFactor(), "eee");
    }
}
