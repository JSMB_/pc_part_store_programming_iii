package com.team8.Sorting;

import java.util.List;

import com.team8.Comparer.ICompare;
import com.team8.Product.Parts;

public class SortingLogic extends ISorter {

    /**
     * Inherited from ISorter interface, this method goes through the list of parts
     * and reorganises it depending on the condition given
     * 
     * @param partsToSort
     * @param condition
     */
    public List<Parts> sortParts(List<Parts> partsToSort, ICompare condition) {
        for (int i = 0; i < partsToSort.size(); i++) {
            for (int k = i + 1; k < partsToSort.size(); k++) {
                if (condition.compareParts(partsToSort.get(i), partsToSort.get(k)) > 0) {
                    Parts swapped = partsToSort.get(i);
                    partsToSort.set(i, partsToSort.get(k));
                    partsToSort.set(k, swapped);
                }
            }
        }

        return partsToSort;
    }
}