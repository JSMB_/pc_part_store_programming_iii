package com.team8.Sorting;

import java.util.List;

import com.team8.Comparer.ICompare;
import com.team8.Product.Parts;

public abstract class ISorter {
   /**
    * Takes a list of parts and sorts them based on logic given by ICompare objects
    * 
    * @param partsToSort
    * @param condition
    */
   public abstract List<Parts> sortParts(List<Parts> partsToSort, ICompare condition);
}