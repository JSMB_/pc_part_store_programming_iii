package com.team8.DataManager;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.team8.Product.*;

import Exception.CustomLoaderException;

public class FileReader implements IDataLoader {
    private String filePath;

    /**
     * Create a fileReader object that takes a file path.
     * 
     * @param filePath
     */
    public FileReader(String filePath) {
        String newFile = filePath;
        this.filePath = newFile;
    }

    /**
     * Changes the filePath field
     * 
     * @param filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * This function read the filePath of the object and through a series of loops
     * and checks that determine wether the item is an internalHardware or a
     * peripherals,
     * makes a List of Parts to use by the object or other.
     * 
     * @Return List<Parts>
     */
    public List<Parts> loadFileData() throws CustomLoaderException {
        Path p = Paths.get(this.filePath);
        List<String> lines = new ArrayList<String>();
        List<Parts> parts = new ArrayList<Parts>();
        try {
            lines.addAll(Files.readAllLines(p));
        } catch (IOException e) {
            throw new CustomLoaderException("Error relating to readAllLines");
        }
        if (lines.get(0).equals("Peripherals")) {
            lines.remove(0);
            for (String line : lines) {
                String[] splitData = line.split(",");

                parts.add(new Peripherals(splitData[0], splitData[1], Double.parseDouble(splitData[2]),
                        Double.parseDouble(splitData[3]), splitData[4], splitData[5], splitData[6]));
            }
        } else if (lines.get(0).equals("Internal Hardware")) {
            lines.remove(0);
            for (String line : lines) {

                String[] splitData = line.split(",");
                parts.add(
                        new InternalHardware(splitData[0], splitData[1], splitData[2], Double.parseDouble(splitData[3]),
                                Double.parseDouble(splitData[4]), splitData[5], splitData[6]));
            }
        }
        return parts;
    }
}
