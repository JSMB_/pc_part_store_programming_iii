package com.team8.DataManager;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.team8.Product.*;

public class FileMaker {

    /**
     * Uses the lists of parts to create a list of products assigned with file name
     * in the products folder. Depending on the typeOfParts, the file will either
     * display internal hardware or peripherals
     * 
     * @param parts
     * @param fileName
     * @param typeOfParts
     */
    public void makeFile(List<Parts> parts, String fileName, String typeOfParts) {
        String fileContent = "";
        fileContent += typeOfParts + "\n";
        for (Parts part : parts) {
            fileContent += part.getName() + "," + part.getManufacturer() + ","
                    + part.getFormFactor() + "," + part.getWattage() + ","
                    + part.getPrice() + "," + part.getType() + ","
                    + part.getConnectionType() + "\n";
        }
        try {
            Files.write(Paths.get(fileName), fileContent.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Uses the a parts object to create a product with file name
     * in the products folder. Depending on the typeOfParts, the file will either
     * display internal hardware or peripherals
     * 
     * @param parts
     * @param fileName
     * @param typeOfParts
     */
    public void makeFile(Parts part, String fileName, String typeOfParts) {
        String fileContent = "";
        fileContent += typeOfParts + "\n";
        fileContent += part.getName() + "," + part.getManufacturer() + ","
                + part.getFormFactor() + "," + part.getWattage() + ","
                + part.getPrice() + "," + part.getType() + ","
                + part.getConnectionType() + "\n";
        try {
            Files.write(Paths.get(fileName), fileContent.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
