package com.team8.DataManager;

import java.util.List;
import com.team8.Product.*;

import Exception.CustomLoaderException;

public interface IDataLoader {

    /**
     * 
     * Basic function that reads the lines from a file and adds it to the list
     * Just adding it in to test the concepts of file read etc
     * 
     * @return allLines from the file that you read as List<Parts>
     */

    public List<Parts> loadFileData() throws CustomLoaderException;
}