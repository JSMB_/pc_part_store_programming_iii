package com.team8.Comparer;

import com.team8.Product.Parts;

public interface ICompare {
    int compareParts(Parts partOne, Parts partTwo);
}
