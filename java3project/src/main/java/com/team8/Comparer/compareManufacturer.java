package com.team8.Comparer;

import com.team8.Product.*;

public class compareManufacturer implements ICompare {
    /**
     * Compares two objects depending on their manufacturer field and returns an int
     * @Return int returned from the comparation. 0 if equals, others if not equals.
     */
    public int compareParts(Parts partOne, Parts partTwo) {
        return (partOne).getManufacturer().compareTo((partTwo).getManufacturer());
    }
}