package com.team8.Comparer;

import com.team8.Product.Parts;

public class compareType implements ICompare {
    /**
     * Compares two objects depending on their type field and returns an int
     * @Return int returned from the comparation. 0 if equals, others if not equals.
     */
    public int compareParts(Parts partOne, Parts partTwo) {
        return partOne.getType().compareTo((partTwo).getType());
    }
}
