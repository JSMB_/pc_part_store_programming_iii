package com.team8.Inventory;

import java.util.ArrayList;
import java.util.List;

import com.team8.Product.InternalHardware;
import com.team8.Product.Parts;
import com.team8.Product.Peripherals;

public class Inventory {

    private List<Parts> storage;

    /**
     * Constructor to initialise storage field with new ArrayList of parts
     */
    public Inventory() {
        this.storage = new ArrayList<Parts>();
    }

    /**
     * Returns an index of the list<Parts>, representing a single Part object
     * 
     * @param index
     * @return Object Parts
     */
    public Parts getStoragePart(int index) {
        return this.storage.get(index);
    }

    /**
     * Sets the values for our Parts List field
     * in the inventory object
     * @param storage Parts list to assign to our object
     */
    public void setStorage(List<Parts> storage) {
        this.storage = storage;
    }

    /**
     * Return the storage containing the list of parts
     * 
     * @return List of parts field
     */
    public List<Parts> getStorage() {
        return this.storage;
    }

    /**
     * Adds a list of parts to the storage field
     * 
     * @param newParts
     */
    public void addParts(List<Parts> newParts) {
        this.storage.addAll(newParts);
    }

    /**
     * Turns storage into readable string format
     */
    public String toString() {
        return this.storage.toString();
    }

    /**
     * Determines the type of parts held within the inventory
     * @return a String containing either "InternalHardware"
     * or "Peripherals"
     */
    public String typeInventory() {
        if (this.storage.get(0) instanceof Peripherals) {
            return "Peripherals";
        } else {
            return "InternalHardware";
        }
    }

    /*
     * Transforms storage data into table format
     */
    public void printPartsListTable() {
        System.out.println();
        System.out.println("Parts List Table");
        System.out.println("-----------------------------");

        System.out.printf("%-20s%-20s%-20s%-20s%-20s%-20s%-20s%n%n", "Name", "Manufacturer",
                "Price ($)", "Wattage",
                "Type", "Connection Type", "Form Factor");

        for (Parts part : this.storage) {
            System.out.printf("%-20s%-20s%-20.2f%-20.2f%-20s%-20s%-20s%n",
                    part.getName(),
                    part.getManufacturer(),
                    part.getPrice(),
                    part.getWattage(),
                    part.getType(),
                    part.getConnectionType(),
                    part.getFormFactor());
        }
    }
}
