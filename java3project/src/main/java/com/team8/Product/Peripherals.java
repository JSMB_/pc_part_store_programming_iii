package com.team8.Product;

public class Peripherals extends Parts {

    private String connectionType;

    private String type;

    private String size;

    /**
     * Gets the type of the peripheral.
     *
     * @return The type of the peripheral.
     */
    public String getType() {
        return this.type;
    }

    /**
     * Gets the connection type of the peripheral.
     *
     * @return The connection type of the peripheral.
     */
    public String getConnectionType() {
        return this.connectionType;
    }

    /**
     * Gets the wattage of the peripheral.
     *
     * @return The wattage of the peripheral.
     */
    public double getWattage() {
        return super.wattage;
    }

    /**
     * Gets the size of the peripheral.
     *
     * @return The size of the peripheral.
     */
    public String getFormFactor() {
        return this.size;
    }

    /**
     * Constructor to initialize the peripheral with the information given
     *
     * @param name
     * @param manufacturer
     * @param wattage
     * @param price
     * @param connectionType
     * @param type
     * @param size
     */
    public Peripherals(String name, String manufacturer, double wattage, double price, String connectionType,
            String type, String size) {
        super(name, manufacturer, wattage, price);
        this.connectionType = connectionType;
        this.type = type;
        this.size = size;
    }

    /**
     * Returns a string representation of the electronic part.
     *
     * @return A string representation of the electronic part.
     */
    public String toString() {
        return super.toString() + ", Connection Type: " + this.connectionType + ", Type: " + this.type + ", Size: "
                + this.size;
    }

    /**
     * Returns a detailed string representation of the electronic part.
     *
     * @return A detailed string representation of the electronic part.
     */
    public String toStringDetails() {
        return super.toStringDetails() + ". You use this product by connecting it to your computer through the "
                + this.connectionType + " port. It is a " + this.type + " device of " + this.size + " size format.";
    }
}
