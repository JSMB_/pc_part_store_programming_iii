package com.team8.Product;

public class InternalHardware extends Parts {

    private String type;

    private String compartement;

    private String formFactor;

    /**
     * Gets the type of the internal hardware.
     *
     * @return The type of the internal hardware.
     */
    public String getType() {
        return this.type;
    }

    /**
     * Gets the compartment of the internal hardware.
     *
     * @return The compartment of the internal hardware.
     */
    public String getConnectionType() {
        return this.compartement;
    }

    /**
     * Gets the form factor of the internal hardware.
     *
     * @return The form factor of the internal hardware.
     */
    public String getFormFactor() {
        return this.formFactor;
    }

    /**
     * Gets the wattage of the internal hardware.
     *
     * @return The wattage of the internal hardware.
     */
    public double getWattage() {
        return super.wattage;
    }

    /**
     * Constructor to initialize the internal hardware with provided information.
     *
     * @param name
     * @param manufacturer
     * @param formFactor
     * @param wattage
     * @param price
     * @param type
     * @param compartement
     */
    public InternalHardware(String name, String manufacturer, String formFactor, double wattage, double price,
            String type, String compartement) {
        super(name, manufacturer, wattage, price);
        this.formFactor = formFactor;
        this.type = type;
        this.compartement = compartement;
    }

    /**
     * Returns a string representation of the electronic part.
     *
     * @return A string representation of the electronic part.
     */
    public String toString() {
        return super.toString() + ", Compartment: " + this.compartement + ", Type: " + this.type + ", Form Factor: "
                + this.formFactor;
    }

    /**
     * Returns a detailed string representation of the electronic part.
     *
     * @return A detailed string representation of the electronic part.
     */
    public String toStringDetails() {
        return super.toStringDetails() + ". You use this product by connecting through " + this.compartement
                + ". It is a " + this.type + " of " + this.formFactor + " size format.";
    }

}
