package com.team8.Product;

public abstract class Parts {
    protected String name;

    protected String manufacturer;

    protected double wattage;

    protected double price;

    /**
     * Gets the manufacturer of the electronic part.
     *
     * @return The manufacturer of the electronic part.
     */
    public String getManufacturer() {
        return this.manufacturer;
    }

    /**
     * Gets the name of the electronic part.
     *
     * @return The name of the electronic part.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the price of the electronic part.
     *
     * @return The price of the electronic part.
     */
    public double getPrice() {
        return this.price;
    }

    /**
     * Gets the type of the electronic part.
     *
     * @return The type of the electronic part.
     */
    public abstract String getType();

    /**
     * Gets the connection type of the electronic part.
     *
     * @return The connection type of the electronic part.
     */
    public abstract String getConnectionType();

    /**
     * Gets the form factor of the electronic part.
     *
     * @return The form factor of the electronic part.
     */
    public abstract String getFormFactor();

    /**
     * Gets the wattage of the electronic part.
     *
     * @return The wattage of the electronic part.
     */
    public abstract double getWattage();

    /**
     * Constructor to initialize the electronic part with provided information.
     *
     * @param name
     * @param manufacturer
     * @param wattage
     * @param price
     */
    public Parts(String name, String manufacturer, double wattage, double price) {
        this.name = name;
        this.manufacturer = manufacturer;
        this.wattage = wattage;
        this.price = price;
    }

    /**
     * Returns a string representation of the electronic part.
     *
     * @return A string representation of the electronic part.
     */
    public String toString() {
        return "Name: " + this.name + ", Manufacturer: " + this.manufacturer + ", Price: " + this.price + "$, Wattage: "
                + this.wattage;
    }

    /**
     * Returns a detailed string representation of the electronic part.
     *
     * @return A detailed string representation of the electronic part.
     */
    public String toStringDetails() {
        return "This is a " + this.name + " made by " + this.manufacturer + ". It uses about " + this.wattage
                + " watts per hour and costs around " + this.price + " dollars";
    }

}
