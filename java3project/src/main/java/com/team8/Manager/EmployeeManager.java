package com.team8.Manager;

import Exception.CustomLoaderException;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.team8.Comparer.ICompare;
import com.team8.Comparer.compareManufacturer;
import com.team8.Comparer.comparePrice;
import com.team8.Comparer.compareType;
import com.team8.DataManager.FileMaker;
import com.team8.DataManager.FileReader;
import com.team8.Inventory.Inventory;
import com.team8.Product.InternalHardware;
import com.team8.Product.Parts;
import com.team8.Product.Peripherals;
import com.team8.Sorting.*;

public class EmployeeManager {

    /**
     * Main method - the main program runs in here
     * aided by all helper methods outside
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Select the file you want to look into");
        List<String> pathNames = getFileList();

        showFileOptions(pathNames);

        int numberChosen = getChoiceFileNb(pathNames.size());

        Inventory invent = new Inventory();

        FileReader reader = new FileReader(pathNames.get(numberChosen));

        try {
            invent.addParts(reader.loadFileData());
        } catch (CustomLoaderException e) {
            e.printStackTrace();
        }

        boolean repeatAgain = true;
        do {
            decideAction(invent);

            boolean invalidChoice = true;

            System.out.println("\nWould you like to continue?"
                                + "\n1. Yes \n2. No \nEnter answer:");
            do {
                String choice = sc.next();
                if(choice.equalsIgnoreCase("yes")) {
                    invalidChoice = false;
                } else if(choice.equalsIgnoreCase("no")) {
                    invalidChoice = false;
                    repeatAgain = false;
                } else {
                    System.out.println("Invalid choice! Please try again:");
                }
            } while(invalidChoice);
        } while(repeatAgain);
    }

    /**
     * Gets all existing files and their paths
     * @return a List of Strings containing the paths to all files
     */
    public static List<String> getFileList() {
        List<String> paths = getPaths();
        return paths;
    }

    /**
     * Displays all possible file choices within the Products
     * folder
     * @param filePaths String List containing all file paths
     * in the Products folder
     */
    public static void showFileOptions(List<String> filePaths) {
        String displayChoice = "";
        for (int i = 0; i < filePaths.size(); i++) {
            displayChoice += "(" + i + ")" + filePaths.get(i) + "\n";
        }
        System.out.println(displayChoice);
    }

    /**
     * Method to select a file on our path depending on
     * the index (it adapts to the amount of files available)
     * @param nbPaths number of files in the directory
     * @return
     */
    public static int getChoiceFileNb(int nbPaths) {
        Scanner input = new Scanner(System.in);
        int numberChosen = 0;
        do {
            numberChosen = input.nextInt();
        } while (numberChosen >= nbPaths || numberChosen < 0);
        return numberChosen;
    }

    /**
     * Looks into the products folder and creates a list of string where each index
     * represents
     * a file present in the folder
     * Based listFiles() from outside source(Internet)
     * 
     * @return list of strings representing the files available to look into
     */
    public static List<String> getPaths() {
        String path = "pc_part_store_programming_iii\\java3project\\src\\csv\\Products";
        File[] files = null;
        File directory = new File(path);
        files = directory.listFiles();
        List<String> paths = new ArrayList<String>();
        for (File file : files) {
            paths.add(file.getPath());
        }
        return paths;
    }

    /**
     * Makes a file for the sorted inventory
     * @param invent our inventory
     * @param compare Compares all items in the inventory
     * based on their fields for the sorting algorithm to work
     * @param wayOfSorting The type of sorting used for our
     * inventory
     */
    public static void sortingProcedure(Inventory invent, ICompare compare, String wayOfSorting) {
        ISorter sort = new SortingLogic();
        Inventory sortedList = new Inventory();
        sortedList.addParts(sort.sortParts(invent.getStorage(), compare));
        String filePath = "pc_part_store_programming_iii\\java3project\\src\\csv\\Products\\";
        String fileName = filePath + "Sorted" + sortedList.typeInventory() + wayOfSorting + ".csv";
        FileMaker fileMaker = new FileMaker();
        fileMaker.makeFile(sortedList.getStorage(), fileName, sortedList.typeInventory());
        sortedList.printPartsListTable();
    }

    /**
     * Provides the user with a set of possible actions
     * they could execute (sort, show inventory, etc.)
     * @param invent the current inventory the user can fiddle with
     */
    public static void decideAction(Inventory invent) {

        Scanner choiceAction = new Scanner(System.in);

        ICompare compare = null;
        String[] actions = { "Sort", "Show data" };
        int numberChosen = -1;

        System.out.println("What would you like to do");
        System.out.println("(0)" + actions[0] + "\n(1)" + actions[1]);
        do {
            System.out.println("Make sure that the number chosen is available in the options");
            numberChosen = choiceAction.nextInt();   
        } while (numberChosen > 2 || numberChosen < 0);
        if (actions[numberChosen].equals("Sort")) {
            System.out.println("How do you want to sort?");
            String sortBy = "";
            do {
                System.out.println("(Enter 'by type', 'by manufacturer', or 'by price')");
                sortBy = choiceAction.nextLine().toLowerCase();
            } while (!sortBy.equals("by type") && !sortBy.equals("by manufacturer") && !sortBy.equals("by price"));

            String wayOfSorting = "";
            switch (sortBy) {
                case "by type":
                    compare = new compareType();
                    wayOfSorting = "ByType";
                    break;
                case "by manufacturer":
                    compare = new compareManufacturer();
                    wayOfSorting = "ByManufacturer";
                    break;
                case "by price":
                    compare = new comparePrice();
                    wayOfSorting = "ByPrice";
                    break;
                default:
                    break;
            }

            sortingProcedure(invent, compare, wayOfSorting);
            System.out.println("Sorted file created");

        } else if (actions[numberChosen].equals("Show data")) {
            invent.printPartsListTable();
        }
    }
}
