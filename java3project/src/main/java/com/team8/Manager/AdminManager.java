package com.team8.Manager;

import Exception.CustomLoaderException;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.io.File;
import java.util.List;
import java.util.Scanner;

import com.team8.Comparer.ICompare;
import com.team8.Comparer.compareManufacturer;
import com.team8.Comparer.comparePrice;
import com.team8.Comparer.compareType;
import com.team8.DataManager.FileMaker;
import com.team8.DataManager.FileReader;
import com.team8.Inventory.Inventory;
import com.team8.Product.*;
import com.team8.Sorting.*;

public class AdminManager {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Select the file you want to look into");
        List<String> pathNames = getFileList();

        showFileOptions(pathNames);

        int numberChosen = getChoiceFileNb(pathNames.size());

        Inventory invent = new Inventory();

        FileReader reader = new FileReader(pathNames.get(numberChosen));

        try {
            invent.addParts(reader.loadFileData());
        } catch (CustomLoaderException e) {
            e.printStackTrace();
        }

        boolean repeatAgain = true;
        do {
            decideAction(invent);

            boolean invalidChoice = true;

            System.out.println("\nWould you like to continue?"
                                + "\n1. Yes \n2. No \nEnter answer:");
            do {
                String choice = sc.next();
                if(choice.equalsIgnoreCase("yes")) {
                    invalidChoice = false;
                } else if(choice.equalsIgnoreCase("no")) {
                    invalidChoice = false;
                    repeatAgain = false;
                } else {
                    System.out.println("Invalid choice! Please try again:");
                }
            } while(invalidChoice);
        } while(repeatAgain);
    }

    public static List<String> getFileList() {
        List<String> paths = getPaths();
        return paths;
    }

    public static void showFileOptions(List<String> filePaths) {
        String displayChoice = "";
        for (int i = 0; i < filePaths.size(); i++) {
            displayChoice += "(" + i + ")" + filePaths.get(i) + "\n";
        }
        System.out.println(displayChoice);
    }

    public static int getChoiceFileNb(int nbPaths) {
        Scanner input = new Scanner(System.in);
        int numberChosen = 0;
        do {
            numberChosen = input.nextInt();
        } while (numberChosen >= nbPaths || numberChosen < 0);
        return numberChosen;
    }

    /**
     * Looks into the products folder and creates a list of string where each index
     * represents
     * a file present in the folder
     * Based listFiles() from outside source(Internet)
     * 
     * @return list of strings representing the files available to look into
     */
    public static List<String> getPaths() {
        String path = "pc_part_store_programming_iii\\java3project\\src\\csv\\Products";
        File[] files = null;
        File directory = new File(path);
        files = directory.listFiles();
        List<String> paths = new ArrayList<String>();
        for (File file : files) {
            paths.add(file.getPath());
        }
        return paths;
    }

    public static void sortingProcedure(Inventory invent, ICompare compare, String wayOfSorting) {
        ISorter sort = new SortingLogic();
        Inventory sortedList = new Inventory();
        sortedList.addParts(sort.sortParts(invent.getStorage(), compare));
        String filePath = "pc_part_store_programming_iii\\java3project\\src\\csv\\Products\\";
        String fileName = filePath + "Sorted" + sortedList.typeInventory() + wayOfSorting + ".csv";
        FileMaker fileMaker = new FileMaker();
        fileMaker.makeFile(sortedList.getStorage(), fileName, sortedList.typeInventory());
        sortedList.printPartsListTable();
    }

    public static void checkExistingFileName(String fileName) {
        int fileCounter = 0;
        for (String fName : getPaths()) {
            if (fName.equals(fileName)) {
                fileCounter++;
                fileName = "pc_part_store_programming_iii\\java3project\\src\\csv\\Products\\New inventory"
                        + fileCounter + ".csv";
            }
        }
    }

    public static void makeFileNewInv() {
        Scanner sc = new Scanner(System.in);

        System.out.println("What name would you like your file to have?");
        String baseName = sc.next();

        System.out.println("How many parts would you like to insert?\n");
        int partsIns = sc.nextInt();

        System.out.println("Peripheral or Hardware? \n(0)Peripheral \n(1)Hardware\n");
        int setUp = sc.nextInt();

        List<Parts> newParts = new ArrayList<>();

        String fileName = "pc_part_store_programming_iii\\java3project\\src\\csv\\Products\\" + baseName + ".csv";

        checkExistingFileName(fileName);

        for (int i = 0; i < partsIns; i++) {
            System.out.println("\n\nPart number " + (i + 1));
            Parts newP = createPart(setUp, sc);
            newParts.add(newP);
        }
        Inventory invent = new Inventory();
        invent.addParts(newParts);
        System.out.println("\nCreating your file...");

        FileMaker newFileInserter = new FileMaker();
        newFileInserter.makeFile(invent.getStorage(), fileName, invent.typeInventory());
    }

    public static Parts createPart(int setUp, Scanner sc) {
        Parts newP;
        String name = null;
        String manufacturer = null;
        double wattage = 0;
        double price = 0;
        String formFactor = null;
        String type = null;
        String compartment = null;
        boolean errorFree = true;
        while (errorFree) {
            try {
                System.out.println("Product name:");
                name = sc.next();
                sc.nextLine();
                System.out.println("Product manufacturer:");
                manufacturer = sc.next();
                sc.nextLine();
                System.out.println("Product wattage:");
                wattage = sc.nextDouble();
                sc.nextLine();
                System.out.println("Product price:");
                price = sc.nextDouble();
                sc.nextLine();
                errorFree = false;
            } catch (InputMismatchException a) {
                System.out.println("Illegal input detected... Please re-enter fields");
                sc.nextLine();
            }
        }

        if (setUp == 0) {
            errorFree = true;
            while (errorFree) {
                try {
                    System.out.println("Product form factor:");
                    formFactor = sc.next();
                    sc.nextLine();
                    System.out.println("Product type:");
                    type = sc.next();
                    sc.nextLine();
                    System.out.println("Product compartment:");
                    compartment = sc.next();
                    sc.nextLine();
                    errorFree = false;
                } catch (InputMismatchException a) {
                    System.out.println("Illegal input detected... Please re-enter fields");
                    sc.nextLine();
                }
            }
            System.out.println("Creating Peripheral...");
            newP = new Peripherals(name, manufacturer, wattage, price, compartment, type, formFactor);
        } else if (setUp == 1) {
            errorFree = true;
            while (errorFree) {
                try {
                    System.out.println("Product form factor:");
                    formFactor = sc.next();
                    sc.nextLine();
                    System.out.println("Product type:");
                    type = sc.next();
                    sc.nextLine();
                    System.out.println("Product compartment:");
                    compartment = sc.next();
                    sc.nextLine();
                    errorFree = false;
                } catch (InputMismatchException a) {
                    System.out.println("Illegal input detected... Please re-enter fields");
                    sc.nextLine();
                }
            }
            System.out.println("Creating Hardware...");
            newP = new InternalHardware(name, manufacturer, formFactor, wattage, price, type, compartment);
        } else {
            throw new IllegalArgumentException("Invalid option. Please try again");
        }

        System.out.println("Part created!");
        return newP;
    }

    // Complete this
    public static void decideAction(Inventory invent) {

        Scanner choiceAction = new Scanner(System.in);

        ICompare compare = null;
        String[] actions = { "Sort", "Show data", "Make file" };
        int numberChosen;

        System.out.println("What would you like to do");
        System.out.println("(0)" + actions[0] + "\n(1)" + actions[1] + "\n(2)" + actions[2] + "\n");
        do {
            System.out.println("Make sure that the number chosen is available in the options");
            numberChosen = choiceAction.nextInt();
        } while (numberChosen > 2 || numberChosen < 0);
        if (actions[numberChosen].equals("Sort")) {
            System.out.println("How do you want to sort?");
            String sortBy = "";
            do {
                System.out.println("(Enter 'by type', 'by manufacturer', or 'by price')");
                sortBy = choiceAction.nextLine().toLowerCase();
            } while (!sortBy.equals("by type") && !sortBy.equals("by manufacturer") && !sortBy.equals("by price"));

            String wayOfSorting = "";
            switch (sortBy) {
                case "by type":
                    compare = new compareType();
                    wayOfSorting = "ByType";
                    break;
                case "by manufacturer":
                    compare = new compareManufacturer();
                    wayOfSorting = "ByManufacturer";
                    break;
                case "by price":
                    compare = new comparePrice();
                    wayOfSorting = "ByPrice";
                    break;
                default:
                    break;
            }

            sortingProcedure(invent, compare, wayOfSorting);
            System.out.println("Sorted file created");

        } else if (actions[numberChosen].equals("Show data")) {
            invent.printPartsListTable();
        } else if (actions[numberChosen].equals("Make file")) {
            makeFileNewInv();
        }

    }
}