package Exception;

public class CustomLoaderException extends Exception {

    public CustomLoaderException() {
    }

    public CustomLoaderException(String message) {
        super(message);
    }
}
